var aa = {
	body: "<p>Android SDK 是 Android 的开发工具包。<a href="
	http: //www.oschina.net/android" rel="nofollow">Android开发专区</a></p> ↵<p><strong>Android是谷歌(Google)公司推出的手机开发平台</strong>。</p> ↵<p>与iPhone相似，Android采用<a href="http://www.oschina.net/p/webkit" rel="nofollow">WebKit</a>浏览器引擎，具备触摸屏、高级图形显示和上网功能，用户能够在手机上查看电子邮件、搜索网址和观看视频节目等，比iPhone等其他手机更强调搜索功能，界面更强大，可以说是一种融入全部Web应用的单一平台，下图是 Android 手机平台开发工具包说提供的模拟器界面截图：</p> ↵<p><img src="http://www.oschina.net/uploads/bbs/2010/0227/103754_w9cA_0.gif" alt="" /></p> ↵<p>但其最震撼人心之处在于Android手机系统的开放性和服务免费。Android是一个对第三方软件完全开放的平台，开发者在为其开发程序时 拥有更大的自由度，突破了iPhone等只能添加为数不多的固定软件的枷锁；同时与Windows Mobile、Symbian等厂商不同，Android操作系统免费向开发人员提供，这样可节省近三成成本。</p> ↵<p>Android项目目前正在从手机运营商、手机厂商、开发者和消费者那里获得大力支持。谷歌移动平台主管安迪&middot;鲁宾(Andy Rubin)表示，与软件开发合作伙伴的密切接触正在进行中。从去年11月开始，谷歌开始向服务提供商、芯片厂商和手机销售商提供Android平台，并 组建“开放手机联盟”，其成员超过30家。</p> ↵<p><img src="http://www.oschina.net/uploads/img/200902/19191516_nuHA.png" alt="" /> <img src="http://www.oschina.net/uploads/img/200902/19191522_qEsj.png" alt="" /> <img src="http://www.oschina.net/uploads/img/200902/19191528_uVgV.png" alt="" /> <img src="http://www.oschina.net/uploads/img/200902/19191535_Cb9k.png" alt="" /></p> ↵<p><img src="http://www.oschina.net/uploads/img/200907/08110848_p1S1.png" alt="" /></p> ↵<p><a href="http://www.osctools.net/apidocs/apidoc?api=android/reference" rel="nofollow"><strong>Android在线API文档</strong></a></p>"
	document: "http://liudong/action/project/go?id=1089&p=doc"
	download: "http://liudong/action/project/go?id=1089&p=download"
	extensionTitle: "Android开发工具包"
	favorite: 0
	homepage: "http://liudong/action/project/go?id=1089&p=home"
	id: 1089
	languages: "Java"
	license: "Apache"
	logo: "http://liudong/img/logo/android.png"
	os: "Android"
	recordtime: "2009-11-22 12:23:17"
	title: "Android SDK"
	url: "http://liudong/p/android"
};