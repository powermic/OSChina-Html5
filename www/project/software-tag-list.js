define(['text!project/software-tag-list.html', "../base/openapi", '../base/util'],
	function(viewTemplate, OpenAPI, Util) {
		return Piece.View.extend({
			id: 'project-software-tag-list',
			events: {
				"click .backBtn": "goBack",
				"click .projectList": "goToSoftDetail",
			},
			goBack: function() {
				history.back();
			},
			goToSoftDetail: function(el) {
				var $target = $(el.currentTarget);
				var url = $target.attr("data-url");
				this.navigate("software-detail?url=" + url, {
					trigger: true 
				});
			},
			render: function() {
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				var tag = Util.request("tag");
				var name = Util.request("name");
				name = decodeURI(name);
				$("#projectTitle").html('');
				$("#projectTitle").html(name);
				Util.loadList(this, 'project-software-tag-list', OpenAPI.project_tag_list, {
					'tag': tag,
					'dataType': OpenAPI.dataType,
					'page': 1,
					'pageSize': OpenAPI.pageSize
				},true);
				//write your business logic here :)
			}
		}); //view define

	});