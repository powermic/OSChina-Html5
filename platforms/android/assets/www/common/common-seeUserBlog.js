define(['text!../common/common-seeUserBlog.html', '../base/userInfo/user', "../base/openapi", '../base/util', "../base/openapi", '../base/login/login'],
	function(viewTemplate, UserInfo, OpenAPI, Util, OpenAPI,Login) {
		return Piece.View.extend({
			login: null,
			userInfo: null,
			id: 'common_common-seeUserBlog',
			events: {
				"click .title": "testUser",
				"click .backBtn": "gobackBtn",
				"click .newsList": "goBlogDetail",
				"click .message": "goMessage",
				"click .tweet": "goTweet"
			},
			goMessage: function() {
				var userOp = Util.request("fromAuthor");
				this.navigate("comment-message?userOp=" + userOp, {
					trigger: true
				});
			},
			goTweet: function() {
				var userOp = Util.request("fromAuthor");
				userOp = "@" + userOp;
				this.navigateModule("tweet/tweet-issue?userOp=" + userOp, {
					trigger: true
				});
			},
			goBlogDetail: function(el) {
				var $target = $(el.currentTarget);
				var id = $target.attr("data-id");
				var fromType = 3;
				var checkDetail = "news/news-blog-detail";
				var com = 5;
				//判断返回  到我的空间或用户信息模块还是  不同的DETAUIL详情
				var from = Util.request("from");

				this.navigateModule("news/news-blog-detail?id=" + id + "&fromType=" + fromType + "&checkDetail=" + checkDetail + "&com=" + com + "&from=" + from, {
					trigger: true
				});
			},
			render: function() {
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				userInfo = new UserInfo();
				login = new Login();

				//write your business logic here :)
				$(".relationLogin").click(function() {
					Util.checkLogin();
					var checkLogin = Util.checkLogin();
					if (checkLogin === false) {
						login.show();
					} else {
						var noRelation = $('.relation').html();
						if (noRelation === "加关注") {
							var token = Piece.Store.loadObject("user_token");
							var user_message = Piece.Store.loadObject("user_message");
							var accesstoken = token.access_token;
							var id = Util.request("fromAuthorId");
							Util.Ajax(OpenAPI.user_relation, "GET", {
								friend: id,
								relation: 1,
								access_token: accesstoken,
								dataType: 'jsonp'
							}, 'json', function(data, textStatus, jqXHR) {
								var yesORno = data.error;
								if (yesORno == 200) {
									new Piece.Toast('您已经对Ta添加关注');
									$('.relation').html("取消关注");


								} else {
									new Piece.Toast(data.error_description);
								}
							}, null);
						} else {
							var token = Piece.Store.loadObject("user_token");
							var user_message = Piece.Store.loadObject("user_message");
							var accesstoken = token.access_token;
							var id = Util.request("fromAuthorId");
							Util.Ajax(OpenAPI.user_relation, "GET", {
								friend: id,
								relation: 0,
								access_token: accesstoken,
								dataType: 'jsonp'
							}, 'json', function(data, textStatus, jqXHR) {
								var yesORno = data.error;
								if (yesORno == 200) {
									new Piece.Toast('您已经对Ta取消关注');
									$('.relation').html("加关注");

								} else {
									new Piece.Toast(data.error_description);
								}
							}, null);
						}



					}
				});
				this.userInfo();



			},
			userInfo: function() {
				var me = this;

				var fromAuthor = Util.request("fromAuthor");
				var fromAuthorId = Util.request("fromAuthorId");
				var token = Piece.Store.loadObject("user_token");

				if (token === null) {
					Util.Ajax(OpenAPI.user_information, "GET", {
						friend: fromAuthorId,
						friend_name: fromAuthor,
						dataType: 'jsonp'
					}, 'json', function(data, textStatus, jqXHR) {
						$('.common-user').html(data.name);
						//user Name
						$('.userName').html(data.name);
						//user Sex
						if (data.gender == 1) {
							$('.userSex').html("男");
						} else {
							$('.userSex').html("女");
						}
						//user Img
						if (data.portrait) {
							$('.userImgContent').attr("src", "../base/img/widget_dface.ing");
						} else {
							$('.userImgContent').attr("src", data.portrait);
						}
						// user From
						$('.userFrom').html(data.province);
						// user City
						$('.userFromCity').html(data.city);
						//join Time
						$('.userJoin').html(data.joinTime.substr(0, 10));
						//platforms
						if (data.platforms) {
							$('.userPlatform').html("<无>");
						} else {
							$('.userPlatform').html(data.platforms);
						}
						//expertise
						if (data.expertise) {
							$('.userSpeciality').html("<无>");
						} else {
							$('.userSpeciality').html(data.expertise);
						}
						//LoginTime
						$('.userLoginTime').html(data.lastLoginTime.substr(0, 10));
						// click relation
						$('.relationLogin').click(function() {

						});

						me.loadList();
					}, null);
				} else {
					var userID = Piece.Store.loadObject("user_message");
					Util.Ajax(OpenAPI.user_information, "GET", {
						user: userID.id,
						friend: fromAuthorId,
						friend_name: fromAuthor,
						dataType: 'jsonp'
					}, 'json', function(data, textStatus, jqXHR) {
						$('.common-user').html(data.name);
						//user Name
						$('.userName').html(data.name);
						//user Sex
						if (data.gender == 1) {
							$('.userSex').html("男");
						} else {
							$('.userSex').html("女");
						}
						//user Img
						if (data.portrait) {
							$('.userImgContent').attr("src", "../base/img/widget_dface.ing");
						} else {
							$('.userImgContent').attr("src", data.portrait);
						}
						// user From
						$('.userFrom').html(data.province);
						// user City
						$('.userFromCity').html(data.city);
						//join Time
						$('.userJoin').html(data.joinTime.substr(0, 10));
						//platforms
						if (data.platforms) {
							$('.userPlatform').html("<无>");
						} else {
							$('.userPlatform').html(data.platforms);
						}
						//expertise
						if (data.expertise) {
							$('.userSpeciality').html("<无>");
						} else {
							$('.userSpeciality').html(data.expertise);
						}
						//LoginTime
						$('.userLoginTime').html(data.lastLoginTime.substr(0, 10));
						//relation
						if (data.relation === 1) {
							$('.relation').html("");
							$('.relation').html("取消互粉");
						} else if (data.relation === 2) {
							$('.relation').html("");
							$('.relation').html("取消互粉");
						} else {
							$('.relation').html("");
							$('.relation').html("加关注");
						}

						me.loadList();
					}, null);
				}
			},
			loadList: function() {
				var user = Util.request("fromAuthor");
				var id = Util.request("fromAuthorId");

				Util.loadList(this, 'my-commentBlog-list', OpenAPI.user_dynmic_blog, {
					'authoruid': id,
					'dataType': OpenAPI.dataType,
					'page': 1,
					'pageSize': OpenAPI.pageSize
				});
			},
			testUser: function() {
				userInfo.show()
				$(".down-list").removeClass("down-list-down");
				$(".down-list").addClass("down-list-up")
			},
			gobackBtn: function() {
				window.history.back();
				
			}
		}); //view define

	});